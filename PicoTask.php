<?php

include_once 'classes/Message.php';
include_once 'functions/GetData.php';
include_once 'functions/DBFunctions.php';
include_once 'functions/CachePico.php';
include_once 'functions/MessageAnalize.php';
include_once 'functions/Websocket.php';

/**
 * all system users
 */
$users;

/**
 * message will store the new Message object with the new data from function NewMessage on GetData.php
 */
$message = NewMessage("twilioData");
$message = NewMessage("facebookData");

/**
 * if the new message was created succesfully , then we can add it to our DB with AddMessageToDB function on
 * DBFunctions.php 
 */
if (!empty($message)){
    $date_added= date("Y-m-d h:i:s");
    if(!AddMessageToDB($message->$accountSid, $message->$data, $message->$status, $date_added)){ 
        echo "<br>" ."could not insert data to DB";
    }
    
}else{
    echo  "<br>" ."invalid data";
}

/**
 * call for cache function CachePico.php
 */
SaveToCacheObj($message->$accountSid,$message);

/**
 * call for AIAnalaiseMessage function , to check if there is any tags in a new message , 
 * get tags array and add it to relaited user
 */
if(!is_null(AIAnalaiseMessage($message->$body))){
    array_push($users[$accountSid]->$tags, $alltags);//$users[$accountSid]?
}

/**
 * calling UpdateSocket to update user socket
 */
UpdateSocket($message->$accountSid,"thank you");

?>