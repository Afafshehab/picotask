<?php

//Message class 
class Message{
    
    public $accountSid;
    public $status;
    public $dateCreated;
    public $body;
    
    public function __construct (){
        
    }

    public function SetMessage ($accountSid,$status,$dateCreated,$body){
        $this->$accountSid = $accountSid;
        $this->$status = $status;
        $this->$dateCreated = $dateCreated;
        $this->$body = $body;
    }
}

?>