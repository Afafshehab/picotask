<?php

//PicoObject class 
class PicoObject{
    
    public $accountSid;
    public $phoneNumber;
    public $firstName;
    public $lastName;
    public $tags= array();//is this neccarry 
    
    public function __construct(){
        
    }

    public function SetPicoObj($accountSid,$phoneNumber,$firstName,$lastName,$tags){
        $this->accountSid = $accountSid;
        $this->phoneNumber = $phoneNumber;
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->tags = $tags;
    }

    public function AddTag($key,$value){
        $tags[$key] = $value;
    }
}

?>