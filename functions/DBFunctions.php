<?php
/**
 * AddUserToDB function , first calls ConnectToDB function to connect to db
 * then if true inserts a new user to the db
 */
function AddMessageToDB($user_id, $data, $status, $date_added) {
    $connection = ConnectToDB();
    if($connection){
        $sqlInsert = "INSERT INTO TableD (user_id, data, status, date_added)
        VALUES ($user_id,$data, $status, $date_added)";
        
        if ($connection->query($sqlInsert)) {
            echo "New record created successfully";
        } else {
            echo "Error: " . $sqlInsert . "<br>" . $connection->error;
            return false;
        }

        $connection -> close();
    }else{
        echo "Connection failed";
        return false;
    }
}

/**
 * ConnectToDB function connect to our DB 
 */
function ConnectToDB() {
    $connection = mysqli_connect("localhost", "username", "password", "myDB");
    
    // Check connection
    if (!$connection) {
        die("Connection failed: " . $connection->connect_error);
        return false;
    }
    echo "Connected successfully";
    return $connection;
}

?>