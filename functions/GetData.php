<?php
    
/**
 * NewMessage function is called to get the new message from a json file , check all requiered params
 * if they are valid , then create a new  message object
 */
function NewMessage ($jsonFile) {
    /**
     * we first get the json file , and then we decode the file and store it 
     */
    $JsonData = json_decode(file_get_contents(dirname(dirname(__FILE__)) . "/json/". $jsonFile.".json"), true);
    
    if(str_icontains($jsonFile,"twilio")){
        $accountSid = $JsonData['messages'][0]['account_sid'];    
        $status = $JsonData['messages'][0]['status'];
        $dateCreated = $JsonData['messages'][0]['date_created'];
        $body = $JsonData['messages'][0]['body'];
    }
    else{
        $accountSid = $JsonData['sender'][0]['id'];    
        $status = "received";
        $dateCreated = $JsonData['timestamp'];
        $body = $JsonData['messages'][0]['text'];
    }


    /**
     * check if all params are valid
     */
    if(!isset($accountSid) || !isset($status) || !isset($dateCreated) || !isset($body)){
        echo 'key values must contain data' . "<br>";
        return false;
    }

    /**
     * create a new message object and pass it through
     */
    $message = new Message();
    $message->SetMessage($accountSid,$status,$dateCreated,$body);
    
    echo 'A new message object was creadted succesfully';
     
    return $message;
}
?>