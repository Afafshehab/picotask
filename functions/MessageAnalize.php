<?php

/**
 * users this is an array of all our users
 * tags is an array of all tags in our system
 */

$tags = array("football" ,"basketball","ski");

/**
 * AIAnalaiseMessage function get a message and check if any of the tags in our system appear in the message 
 * if yes the value get added to a tags array 
 * return tags array
 */
Function AIAnalaiseMessage($body){
    $messageTags = null;
    foreach ($tags as $alltags){
        if(preg_match("/{$alltags}/", $body)){
            array_push($messageTags, $alltags);
        }
    }
    return $messageTags;
}


?>