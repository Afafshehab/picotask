<?php

/**
 * sockets is the array that contains all available sockets
 */
$sockets;

/**
 * UpdateSocket function findes the socket that is attached to the user and with socket_write send the 
 * respoce to the user
 */
function UpdateSocket($accountSid,$responce){
    foreach($sockets as $sok){
        if($sok == $accountSid){
            socket_write($sok, $responce, strlen($responce));
        }
    }
}

?>